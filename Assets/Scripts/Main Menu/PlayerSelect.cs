﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XboxCtrlrInput;
using UnityEngine.SceneManagement;

public class PlayerSelect : MonoBehaviour {

	public bool[] players = new bool[4];
    public GameObject[] startPrompts = new GameObject[4];
    public GameObject[] greyouts = new GameObject[4];
    public GameObject startPlaying;
    bool hasStarted = false;

    public StartOptions startOptions;
	// Use this for initialization
	void Start () {

        DontDestroyOnLoad(this.gameObject);
	}

    // Update is called once per frame
    void Update()
    {
        CheckPlayerInputs();

        if (XCI.GetButtonDown(XboxButton.Start, XboxController.Any))
        {
            for (int i = 0; i < players.Length; i++)
            {
                if (players[i])
                {
                    if (!hasStarted)
                    {
                        hasStarted = true;
                        startOptions.StartButtonClicked();
                    }
                    //SceneManager.LoadScene("MainGame");
                }
            }

        }
    }

	void CheckPlayerInputs()
	{
		if (XCI.GetButtonDown(XboxButton.A, XboxController.First))
		{
			players[0] = true;
            startPrompts[0].SetActive(false);
            greyouts[0].SetActive(false);
        }

		if (XCI.GetButtonDown(XboxButton.B, XboxController.First))
		{
			players[0] = false;
            startPrompts[0].SetActive(true);
            greyouts[0].SetActive(true);
        }

		if (XCI.GetButtonDown(XboxButton.A, XboxController.Second))
		{
			players[1] = true;
            startPrompts[1].SetActive(false);
            greyouts[1].SetActive(false);
        }

		if (XCI.GetButtonDown(XboxButton.B, XboxController.Second))
		{
			players[1] = false;
            startPrompts[1].SetActive(true);
            greyouts[1].SetActive(true);
        }

		if (XCI.GetButtonDown(XboxButton.A, XboxController.Third))
		{
			players[2] = true;
            startPrompts[2].SetActive(false);
            greyouts[2].SetActive(false);
        }

		if (XCI.GetButtonDown(XboxButton.B, XboxController.Third))
		{
			players[2] = false;
            startPrompts[2].SetActive(true);
            greyouts[2].SetActive(true);
        }

		if (XCI.GetButtonDown(XboxButton.A, XboxController.Fourth))
		{
			players[3] = true;
            startPrompts[3].SetActive(false);
            greyouts[3].SetActive(false);
        }

		if (XCI.GetButtonDown(XboxButton.B, XboxController.Fourth))
		{
			players[3] = false;
            startPrompts[3].SetActive(true);
            greyouts[3].SetActive(true);
        }
	}
}
