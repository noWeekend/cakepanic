﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using XboxCtrlrInput;

public class PlayerMovement : MonoBehaviour {

	//Player variables
	[SerializeField] private XboxCtrlrInput.XboxController xboxController;

	[SerializeField] public int playerNum;

	[SerializeField] private float groundSpeed = 40;
	[SerializeField] private float airSpeed = 40;
	[SerializeField] private float slowDownSpeedGround = 0.85f;
	[SerializeField] private float slowDownSpeedAir = 0.95f;
	[SerializeField] private float maxSpeed = 10;
	[SerializeField] private float jumpHeight = 8;
	[SerializeField] private float coyoteJumpDelay = 0.2f;
	[SerializeField] private float fallSpeed = 7.15f;

	private Rigidbody rigidBody;
	public bool isGrounded = false;

	//Platter Movement
	[SerializeField] public float platterMaxRotation = 20f;
	[SerializeField] public float platterMinRotation = -20f;
	[SerializeField] public float moveRotationSpeed = 20;
	[SerializeField] public float restoreRotationSpeed = 5;

    //Cake Throwing
    [SerializeField] private KeyCode throwKey;
    [SerializeField] private float throwForceHorizontal = 2f;
    [SerializeField] private float throwForceVertical = 1f;
    [SerializeField] private float tierThrowDelay = 0.02f;
    [SerializeField] private Transform facingCheck;

	private float currentRotation = 0;

	[SerializeField] private GameObject body;
	private CapsuleCollider bodyCollider;
	private float bodyScale;

	//Cake Management
	public List<CarryObject> carryingCakeObjects = new List<CarryObject>();
	public Transform cakePlatter;

	public bool usesKeyboard = false;

	public Transform spawnPoint;
	public float inactiveTime = 60;
	private float inactiveTimer = 0;
	public bool isActive = false;

    private CakePlate cakePlate;

    public bool jumpInputBuffer = false;
    public float jumpInputBufferTimer = 0.5f;
    public float jumpInputTimer = 0;

    public int CakeCount { get { return carryingCakeObjects.Count; } }

    public bool onUpJump = false;

    public bool hideOnStart =false ;

	public void AddCarryObject(CarryObject carryObject) {
		carryingCakeObjects.Add(carryObject);
		carryObject.transform.parent = cakePlatter;
	}

	public void RemoveCarryObject(CarryObject carryObject) {

	}

	private void Awake()
	{
		rigidBody = GetComponent<Rigidbody>();
		bodyCollider = GetComponent<CapsuleCollider>();
		bodyScale = body.transform.localScale.x;

        cakePlate = cakePlatter.GetComponent<CakePlate>();

		switch (xboxController) {
		case XboxController.First:
			playerNum = 1;
			break;

		case XboxController.Second:
			playerNum = 2;
			break;

		case XboxController.Third:
			playerNum = 3;
			break;

		case XboxController.Fourth:
			playerNum = 4;
			break;

		default:
			Debug.Log("Player isn't set to 1,2,3 or 4.  UI won't work properly");
			break;
		}

	}

    private void Update() {

		if (isActive)
		{
			UpdateFacing();

			if (XCI.GetButtonDown(XboxButton.X, xboxController) || Input.GetKeyDown(throwKey))
			{
				Throw();
			}

			inactiveTimer += Time.deltaTime;
			if (inactiveTimer >= inactiveTime)
			{
				HidePlayer();
			}

            bool hasHitGround = false;

            for (int i = 0; i < 3; i++)
            {

                RaycastHit hit;

                Ray downRay = new Ray(transform.position + (Vector3.up * 0.2f) - (Vector3.left * 0.2f) + (Vector3.left * 0.2f * i), Vector3.down);
                Debug.DrawRay(transform.position - (Vector3.up * -0.2f) - (Vector3.left * 0.2f) + (Vector3.left * 0.2f * i), Vector3.down, Color.yellow);

                if (Physics.Raycast(downRay, out hit, 0.3f))
                {
                    hasHitGround = true;
                }
            }

            if (hasHitGround)
            {
                isGrounded = true;
            }
            else
            {
                if (isGrounded)
                {
                    Invoke("JumpDelay", coyoteJumpDelay);
                }
            }

            if (jumpInputBuffer)
            {
                jumpInputTimer += Time.fixedDeltaTime;
                if (jumpInputTimer >= jumpInputBufferTimer)
                {
                    jumpInputBuffer = false;
                }

            }
        }
		else
		{
			if (XCI.GetButtonDown(XboxButton.Start, xboxController))
			{
				ResetPlayer();
			}
		}


	}

	public void Initilise(Transform startPoint)
	{
		spawnPoint = startPoint;
		ResetPlayer();
	}

	void FixedUpdate () {
		if (isActive)
		{
			CharacterMovement();
		}
	}

	//Move the Character
	void CharacterMovement()
	{



		Jump();

		//If the player isn't grounded, and it's moving slow enough, apply gravity
		if (!isGrounded)
		{
			rigidBody.AddForce(Vector3.down * fallSpeed);

            if (rigidBody.velocity.y < 0)
            {
                gameObject.layer = 10;
            }
        }
        
		//Movement 
		float moveHorizontal = XCI.GetAxis(XboxAxis.LeftStickX, xboxController);

		if (moveHorizontal != 0)
		{
			inactiveTimer = 0;
		}

		// Debug keyboard movement
		if (usesKeyboard)
		{
			moveHorizontal = Input.GetAxis("Horizontal");
		}

		Vector3 currectVelocity = rigidBody.velocity;

		float modifier = 1;
		if ((currectVelocity.x < 0 & moveHorizontal > 0 ) || (currectVelocity.x > 0 & moveHorizontal < 0))
		{
            if (isGrounded)
            {
                rigidBody.velocity = new Vector3(0, rigidBody.velocity.y, 0);
            }
		}

		float movementSpeed = groundSpeed;
		if (!isGrounded)
		{
			movementSpeed = airSpeed;
		}

		Vector3 movementForce = Vector3.right * moveHorizontal * movementSpeed * modifier;
		if (currectVelocity.x > maxSpeed)
		{
			rigidBody.velocity = new Vector3(maxSpeed, currectVelocity.y, 0);
		} else if (currectVelocity.x < -maxSpeed) {
			rigidBody.velocity = new Vector3(-maxSpeed, currectVelocity.y, 0);
		}

		rigidBody.AddForce(movementForce);

		if (moveHorizontal == 0)
		{
			if (isGrounded)
			{
				rigidBody.velocity = new Vector3(currectVelocity.x * slowDownSpeedGround, currectVelocity.y);
			}
			else
			{
				rigidBody.velocity = new Vector3(currectVelocity.x * slowDownSpeedAir, currectVelocity.y);
			}
		}


		PlatterMovement(moveHorizontal);
	}

	void JumpDelay()
	{
		isGrounded = false;
	}

	public void HidePlayer()
	{
		Throw();
		bodyCollider.enabled = false;
		body.SetActive(false);
		if (UIController.instance && (playerNum == (1) ||playerNum == (2) ||playerNum == (3) ||playerNum == (4))) UIController.instance.DeactivatePlayer (playerNum);
		isActive = false;
		cakePlatter.gameObject.SetActive(false);
	}

	void ResetPlayer()
	{
		transform.position = spawnPoint.position;
		bodyCollider.enabled = true;
		body.SetActive(true);
		isActive = true;
		inactiveTimer = 0;
				if (UIController.instance && (playerNum == (1) ||playerNum == (2) ||playerNum == (3) ||playerNum == (4))) UIController.instance.ActivatePlayer (playerNum);
		cakePlatter.gameObject.SetActive(true);
	}

	//Jump
	void Jump()
	{
        if (jumpInputBuffer)
        {
            if (isGrounded)
            {
                jumpInputBuffer = false;
                jumpInputTimer = 0;
                AddJumpForce();

            }
        }
        else
        {
            if (XCI.GetButtonDown(XboxButton.A, xboxController) || Input.GetKeyDown(KeyCode.Space))
            {
                if (!isGrounded)
                {
                    jumpInputBuffer = true;
                    jumpInputTimer = 0;
                }
                else
                {
                    AddJumpForce();
                }
                
            }


           
        }


    }

    void AddJumpForce()
    {
            inactiveTimer = 0;
            isGrounded = false;
        //return Vector3.up * jumpHeight;
        rigidBody.velocity = new Vector3(rigidBody.velocity.x, 0);
            rigidBody.AddForce(Vector3.up * jumpHeight, ForceMode.Impulse);

            gameObject.layer = 11;
    }

	//Platter Movement
	void PlatterMovement(float movementValue)
	{
	
		if (movementValue != 0)
		{
			currentRotation += moveRotationSpeed * movementValue ;
		}
		else
		{
			currentRotation = Mathf.MoveTowards(currentRotation, 0, restoreRotationSpeed);
		}

		currentRotation = Mathf.Clamp(currentRotation, platterMinRotation, platterMaxRotation);
		cakePlatter.rotation = Quaternion.Euler(new Vector3(0, 0, currentRotation));
	}

    private void Throw() {
        Vector3 throwVelocity = new Vector3(throwForceHorizontal, throwForceVertical, 0f);

        if (facingCheck.transform.localScale.x < 0)
            throwVelocity.x = -throwVelocity.x;

        StartCoroutine(ThrowCake(throwVelocity));

        if(cakePlate)
            cakePlate.CakeThrown();
    }

    private void UpdateFacing() {
        if (rigidBody.velocity.x < 0 && facingCheck.transform.localScale.x > 0) {
            facingCheck.transform.localScale = new Vector3(-facingCheck.transform.localScale.x,
                facingCheck.transform.localScale.y,
                facingCheck.transform.localScale.z);
			body.transform.localScale = Vector3.one * bodyScale;
		} else if (rigidBody.velocity.x > 0 && facingCheck.transform.localScale.x < 0) {
            facingCheck.transform.localScale = new Vector3(-facingCheck.transform.localScale.x,
                facingCheck.transform.localScale.y,
                facingCheck.transform.localScale.z);
			body.transform.localScale = new Vector3(-1, 1, 1) * bodyScale;
		}
    }

    private IEnumerator ThrowCake(Vector3 throwVelocity) {

        List<CarryObject> cakeList = new List<CarryObject>();

        foreach (Transform child in cakePlatter)
        {
            cakeList.Add(child.GetComponent<CarryObject>()); ;
        }



        for (int i = cakeList.Count - 1; i >= 0; i--) {
            cakeList[i].MoveObject(throwVelocity);
            cakeList[i].transform.SetParent(null);

            yield return new WaitForSeconds(tierThrowDelay);
        }

        carryingCakeObjects.Clear();
    }
}
