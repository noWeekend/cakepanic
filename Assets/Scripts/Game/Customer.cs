﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Customer : MonoBehaviour {

	public int score;	//Send this to some score manager on the game manager

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnCollisionEnter (Collision collision)
	{
		if (collision.transform.tag == "Cake") 
		{
            int lastPlayer = collision.transform.GetComponent<CarryObject>().lastPlayerTouched;
            Destroy(collision.transform.gameObject);
            if (lastPlayer == 99)
            {

                return;
            }

            score++;
			if (UIController.instance) UIController.instance.SetScore (lastPlayer);
			

		}
			
	}
}
