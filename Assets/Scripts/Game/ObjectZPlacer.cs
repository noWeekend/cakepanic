﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectZPlacer : MonoBehaviour {
    private float randomiser;
	public float modifier = 0.01f;
    // Update is called once per frame

    private void Awake()
    {
        randomiser = Random.Range(0.00001f, 0.01f);
    }

    void Update () {

		transform.position = new Vector3(transform.position.x, transform.position.y, (transform.position.y * -modifier) + randomiser);

	}
}
