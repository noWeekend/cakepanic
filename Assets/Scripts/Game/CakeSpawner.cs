﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CakeSpawner : MonoBehaviour {

	public List<GameObject> cakePieces;
	public float multiplierX = 5;
	public float multiplierY = 5;
	public float minCakeSpawnTime = 3;
	public float maxCakeSpaenTime = 5;

	void Awake(){
		Invoke ("SpawnCakePieces", 0);
	}

	void SpawnCakePieces(){
		Vector3 cakeVector = new Vector3 (Random.Range (-1, 1) * multiplierX, Random.Range (0.5f, 1) * multiplierY, 0);
		GameObject randomCakePiece = cakePieces [Random.Range (0, cakePieces.Count)];
		GameObject newCakePiece = Instantiate (randomCakePiece, transform.position, Quaternion.identity) as GameObject;
		newCakePiece.GetComponent<Rigidbody> ().AddForce (cakeVector,ForceMode.Impulse);
		Invoke ("SpawnCakePieces", Random.Range (minCakeSpawnTime, maxCakeSpaenTime));
	}

}
