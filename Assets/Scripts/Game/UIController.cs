﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

	public static UIController instance;

	public GameObject[] activeUI;
    public int[] playerScore = new int[4];
	public GameObject[] inactiveUI;
	public Text scoreText;


	// Use this for initialization
	void Awake () {
		if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy (gameObject);
		DontDestroyOnLoad (gameObject);

		foreach (GameObject item in inactiveUI)
		{
			item.SetActive(false);
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ActivatePlayer(int i)
	{
		activeUI [i -1].SetActive (true);
		inactiveUI [i -1].SetActive (false);
        playerScore[i - 1] = 0;

    }

	public void DeactivatePlayer(int i)
	{
		activeUI [i -1].SetActive (false);
		inactiveUI [i -1].SetActive (true);
	}

	public void SetScore(int playerNumber)
	{
        playerScore[playerNumber - 1]++;
        activeUI[playerNumber - 1].GetComponent<Text>().text = "Player " + playerNumber + " Score: " + playerScore[playerNumber - 1];
	}
}
