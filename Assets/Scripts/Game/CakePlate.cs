﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CakePlate : MonoBehaviour {
    [SerializeField] private string cakeTag;
    [SerializeField] private float yHeightAdjustment;
    [SerializeField] private int maxCake;

    private PlayerMovement player;
    private BoxCollider triggerCollider;
    private Vector3 originalTriggerCenter;

    private void Start() {
        player = GetComponentInParent<PlayerMovement>();
        triggerCollider = GetComponents<BoxCollider>().ToList().Find(t => t.isTrigger);

        originalTriggerCenter = triggerCollider.center;
    }

    public void CakeThrown() {
        triggerCollider.center = originalTriggerCenter;
    }

	private void OnTriggerStay(Collider other) {
        if(other.tag == cakeTag && other.GetComponent<Rigidbody>().isKinematic == false) {
            CarryObject cake = other.GetComponent<CarryObject>();

            if (cake && cake.CanAttach && player.CakeCount < maxCake) {
                Vector3 center = triggerCollider.center;
                center.y -= yHeightAdjustment; 

                player.AddCarryObject(cake);
                cake.StartCarry(center,player.playerNum);

                BoxCollider b = other as BoxCollider;

                Vector3 heightAdjustment = Vector3.zero;
                heightAdjustment.y += b.size.y;

                triggerCollider.center += heightAdjustment;
            }
        }
    }
}
