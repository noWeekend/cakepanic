﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarryObject : MonoBehaviour
{
    [SerializeField] private float pushAwayVelocity;
    [SerializeField] private float pushAwayLength;
    [SerializeField] private LayerMask cakeMask;
    [SerializeField] private float centreSlideSpeed;

    [SerializeField] private float attachmentCooldownTime;

    public bool CanAttach { get; private set; }

    Rigidbody rigidBody;
    Collider col;
    public Vector3 touchingPosition = Vector3.zero;

    private float colliderHeight;

    private Transform rotationPoint;
    private Vector3 slideToPoint;
    private bool rotationStarted;
    private float attachCount;

    public float timer = 0;
    public float maxInactiveTimer = 2;

    public int lastPlayerTouched = 99;

    void Start() {
        rigidBody = GetComponent<Rigidbody>();
        col = GetComponent<Collider>();

        colliderHeight = transform.localScale.y * col.bounds.size.y;
        slideToPoint = transform.localPosition;

        if(transform.parent == null || transform.parent.GetComponent<CakePlate>() == null)
            CanAttach = true;
    }

    private void Update() {
        UpdateAttachmentCooldown();
        PushAway();
        SlideToCenter();
        CheckDestory();
    }

    private void CheckDestory()
    {
        if (rigidBody.isKinematic)
        {
            return;
        }

        if (rigidBody.velocity.magnitude > 0.5f)
        {
            timer = 0;
            return;
        }

        timer += Time.deltaTime;

        if (timer >= maxInactiveTimer)
        {
            DestoryObject();
        }
    }

    private void DestoryObject()
    {
        Destroy(this.gameObject);
    }


    public virtual void StartCarry(Vector3 centerPoint,int playerNumber) {
        CanAttach = false;
        attachCount = 0f;

        slideToPoint = centerPoint;
        rigidBody.isKinematic = true;
        lastPlayerTouched = playerNumber;
    }

    public virtual void MoveObject(Vector3 velocity) {
        if (rigidBody == null)
            return;

        if (rigidBody.isKinematic)
            rigidBody.isKinematic = false;

        rigidBody.velocity = velocity;
    }

    public virtual void RotateObject(Vector3 angularVelodity) {
        if (rigidBody == null || rigidBody.isKinematic)
            return;

        rigidBody.AddTorque(transform.forward + angularVelodity, ForceMode.Acceleration);
    }

    public virtual void PushObject(Vector3 velocity) {
        if (rigidBody == null)
            return;

        rigidBody.AddForce(velocity, ForceMode.VelocityChange);
    }

    private void StartRotation() {
        rotationStarted = true;
    }

    private void UpdateAttachmentCooldown() {
        if (CanAttach == false && rigidBody.isKinematic == false) {
            attachCount += Time.deltaTime;

            if (attachCount >= attachmentCooldownTime) {
                attachCount = 0f;
                CanAttach = true;
            }
        }
    }

    private void PushAway() {
        if (rigidBody.isKinematic)
            return;

        RaycastHit hit;
        Physics.Raycast(col.bounds.center + new Vector3(0, (colliderHeight / 2) + 0.01f), transform.up, out hit, pushAwayLength, cakeMask);

        Debug.DrawRay(col.bounds.center + new Vector3(0, (colliderHeight / 2) + 0.01f), transform.up * pushAwayLength, Color.red);

        if (hit.transform != null && hit.transform != transform) {
            CarryObject hitCake = hit.transform.GetComponent<CarryObject>();

            if (hitCake != null) {
                hitCake.PushObject(new Vector3(0f, pushAwayVelocity, 0f));
            }
        }
    }

    private void SlideToCenter() {
        if (rigidBody == null || rigidBody.isKinematic == false || transform.localPosition == slideToPoint)
            return;

        float step = centreSlideSpeed * Time.deltaTime;

        transform.localPosition = Vector3.MoveTowards(transform.localPosition, slideToPoint, step);
        transform.localRotation = Quaternion.Slerp(transform.localRotation, Quaternion.identity, step);
    }

    private void OnCollisionEnter(Collision collision) {
        Vector3 touchPosition = Vector3.zero;
        foreach (ContactPoint contactPoint in collision.contacts) {
            touchPosition += contactPoint.point;
        }

        touchingPosition /= collision.contacts.Length;
    }

    //private void OnDrawGizmos() {
    //    Gizmos.color = Color.red;
    //    Gizmos.DrawSphere(transform.position + touchingPosition, 0.1f);
    //}

}
