﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour {

    List<CarryObject> staringObjects = new List<CarryObject>();

    public float cakeFallingDelay = 3;

	// Use this for initialization
	void Start () {

        Invoke("StartCakeFalling", cakeFallingDelay);

        foreach (CarryObject carryObject in FindObjectsOfType<CarryObject>())
        {
            if (!carryObject.GetComponent<Rigidbody>().isKinematic)
            {
                carryObject.GetComponent<Rigidbody>().isKinematic = true;
                staringObjects.Add(carryObject);
            }
        }

        GameObject[] spawnPoints = GameObject.FindGameObjectsWithTag("SpawnPoint");
		PlayerMovement[] allPlayers = FindObjectsOfType<PlayerMovement>();


		if (spawnPoints.Length != 4) {
			Debug.LogWarning("There should be 4 spawn points in this level");
		}

		if (allPlayers.Length != 4)
		{
			Debug.LogWarning("There should be 4 players in this level");
		}

		for (int i = 0; i < allPlayers.Length; i++)
		{
			allPlayers[i].Initilise(spawnPoints[i].transform);
		}

        PlayerSelect playerselect = FindObjectOfType<PlayerSelect>();

        if (playerselect)
        {
            for (int i = 0; i < 4; i++)
            {
                if (!playerselect.players[i])
                {
                    foreach (PlayerMovement player in allPlayers)
                    {
                        if(player.playerNum == i+1)
                        {
                            player.HidePlayer();
                        }
                    }
                    
                }
            }
        }
    }


    void StartCakeFalling()
    {
        foreach (CarryObject carryObject in staringObjects)
        {
            carryObject.GetComponent<Rigidbody>().isKinematic = false;
        }

    }

    // Update is called once per frame
    void Update () {
		
	}
}
