﻿using UnityEngine;
using System.Collections;
using XboxCtrlrInput;

[RequireComponent (typeof (Player))]
public class PlayerInput : MonoBehaviour {

	Player player;
	private bool useKeyboard = true;
	private bool isRunning = false;

	void Start () {
		player = GetComponent<Player> ();
	}

	void Update () {

		Vector2 directionalInput = new Vector2 (Input.GetAxisRaw ("Horizontal"), Input.GetAxisRaw ("Vertical"));
		player.SetDirectionalInput (directionalInput);

		if (Input.GetKeyDown(KeyCode.Space) || XCI.GetButtonDown(XboxButton.A,XboxController.All)) {
			player.OnJumpInputDown ();
		}

		if (Input.GetKeyUp(KeyCode.Space) || XCI.GetButtonUp(XboxButton.A,XboxController.All)) {
			player.OnJumpInputUp ();
		}

		if (useKeyboard) {
			if (Input.GetKeyDown (KeyCode.LeftShift)){
				player.Run ();
			}

			if (Input.GetKeyUp (KeyCode.LeftShift)){
				player.StopRun ();
			}
			
			if(XCI.GetAxis(XboxAxis.RightTrigger,XboxController.All) > 0){
				useKeyboard = false;
			}

		} else {
			if (XCI.GetAxis (XboxAxis.RightTrigger, XboxController.All) > 0) {
				player.Run ();
				isRunning = true;
			}

			if (XCI.GetAxis (XboxAxis.RightTrigger, XboxController.All) == 0 && isRunning ==true) {
				player.StopRun ();
				isRunning = false;
			}

			if(Input.GetKeyDown (KeyCode.LeftShift)){
				player.Run ();
				useKeyboard = true;
			}
		}
//
//
//		if (Input.GetKeyDown (KeyCode.LeftShift) || XCI.GetAxis(XboxAxis.RightTrigger,XboxController.All) > 0) {
//			player.Run ();
//		}else if (Input.GetKeyUp (KeyCode.LeftShift) || XCI.GetAxis(XboxAxis.RightTrigger,XboxController.All) == 0){
//			player.StopRun ();
//			Debug.Log ("bye");
//		}
//			

	}
}
