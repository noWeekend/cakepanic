﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (Controller2D))]
public class Player : MonoBehaviour {

	public Transform gunPosition;
	public GameObject shellCasePrefab;
	public GameObject bigShellCasePrefab;
	public GameObject smallBulletPrefab;
	public GameObject bigBulletPrefab;
	private GameController GC;
	public GameObject playerBody;
	public GameObject playerGroup;

	private AudioSource AS;
	public AudioSource GunAS;

	public AudioClip lightGunFire;
	public AudioClip heavyGunFire;

	private float playerFacing = 1;
	public ParticleSystem slidingParticles;
	public ParticleSystem fallingParticles;
	public ParticleSystem wallSlideParticles;
	public ParticleSystem wallJumpParticles;
	public ParticleSystem JumpParticles;
	public ParticleSystem MuzzleFlashParticles;
	public float fallingParticlesMaxFallSpeed = -0.5f;

	public float maxJumpHeight = 4;
	public float minJumpHeight = 1;
	public float timeToJumpApex = .4f;
	float accelerationTimeAirborne = .2f;
	float accelerationTimeGrounded = .1f;
	public float moveSpeed = 6;
	public float runSpeed = 10;
	private float walkSpeed = 6;
	public float shootSpeed = 1000;

	public Vector2 wallJumpClimb;
	public Vector2 wallJumpOff;
	public Vector2 wallLeap;

	public float wallSlideSpeedMax = 3;
	public float wallStickTime = .25f;
	float timeToWallUnstick;

	float gravity;
	float maxJumpVelocity;
	float minJumpVelocity;
	Vector3 velocity;
	Vector3 oldVelocity;
	float velocityXSmoothing;

	Controller2D controller;

	Vector2 directionalInput;
	bool wallSliding;
	int wallDirX;

	private Vector3 startPosition;
	void Start() {
		controller = GetComponent<Controller2D> ();

		gravity = -(2 * maxJumpHeight) / Mathf.Pow (timeToJumpApex, 2);
		maxJumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
		minJumpVelocity = Mathf.Sqrt (2 * Mathf.Abs (gravity) * minJumpHeight);
		GC = GameObject.Find ("GameController").GetComponent<GameController> ();
		AS = GetComponent<AudioSource> ();
		startPosition = transform.position;
	}

	void Update() {

		if (transform.position.y < -20) {
			transform.position = startPosition;
            velocity = Vector3.zero;
        }
		CalculateVelocity ();
		HandleWallSliding ();

		controller.Move (velocity * Time.deltaTime, directionalInput);

		if (controller.collisions.above || controller.collisions.below) {
			if (controller.collisions.slidingDownMaxSlope) {
				velocity.y += controller.collisions.slopeNormal.y * -gravity * Time.deltaTime;

				if (controller.collisions.slopeNormal.x > 0) {
					playerFacing = 1;
				} else {
					playerFacing = -1;
				}

                if (slidingParticles)
                {
                    if (slidingParticles.isStopped)
                    {
                        slidingParticles.Play();
                    }
                }

				//Debug.Log ("hi");
			} else {
				velocity.y = 0;
                if (slidingParticles)
                {
                    if (slidingParticles.isPlaying)
                    {
                        slidingParticles.Stop();
                    }
                }
			}
		} else {
            if (slidingParticles)
            {
                if (slidingParticles.isPlaying)
                {
                    slidingParticles.Stop();
                }
            }
		}
		//Debug.Log(velocity.y);

		if (velocity.y == 0 && oldVelocity.y < fallingParticlesMaxFallSpeed) {
			//Debug.Log(oldVelocity.y);

			fallingParticles.Play ();

			if (wallSlideParticles.isPlaying) {
				wallSlideParticles.Stop ();
			}

			AS.pitch = Random.Range (0.9f, 1.1f);
			AS.Play ();
		}

		if (velocity.y > 0) {
			if (wallSlideParticles.isPlaying) {
				wallSlideParticles.Stop ();
			}
		}
			
		oldVelocity = velocity;


		float stretchValue = 1;
		////SquashAndStretch				// REPLACE WITH PLAYING AN ANIMATION
		//if ((GC.useSquashAndStretch) && (velocity.y > 0)) {
		//	stretchValue = Mathf.Clamp (Mathf.Abs (velocity.y) / 8, 1, 1.3f);
		//}
		//	playerBody.transform.localScale = new Vector3 (
		//		1/stretchValue,
		//		stretchValue, 
		//		1
		//	);

		//playerGroup.transform.localScale = new Vector3 (playerBody.transform.localScale.x * playerFacing,playerBody.transform.localScale.y, playerBody.transform.localScale.z);
	}

	public void Run(){
		moveSpeed = runSpeed;
		wallSlideParticles.Play ();
	}

	public void StopRun(){
		moveSpeed = walkSpeed;
		wallSlideParticles.Stop ();
	}

	public void ChangeGunSounds(bool useHeavy){
		if (useHeavy) {
			GunAS.clip = heavyGunFire;
		} else {
			GunAS.clip = lightGunFire;
		}
	}

	public void SetDirectionalInput (Vector2 input) {
		directionalInput = input;
		if (directionalInput.x > 0) {
			playerFacing = 1;
		} else if (directionalInput.x < 0){
			playerFacing = -1;
		}
	}

	public void OnJumpInputDown() {
		if (wallSliding)
		{
			if (wallDirX == directionalInput.x)
			{
				velocity.x = -wallDirX * wallJumpClimb.x;
				velocity.y = wallJumpClimb.y;
			}
			else if (directionalInput.x == 0)
			{
				velocity.x = -wallDirX * wallJumpOff.x;
				velocity.y = wallJumpOff.y;
			}
			else
			{
				velocity.x = -wallDirX * wallLeap.x;
				velocity.y = wallLeap.y;
			}

			if (wallJumpParticles.isStopped)
				wallJumpParticles.Play ();
		}
		if (controller.collisions.below) {
			if (controller.collisions.slidingDownMaxSlope) {
				if (directionalInput.x != -Mathf.Sign (controller.collisions.slopeNormal.x)) { // not jumping against max slope
					velocity.y = maxJumpVelocity * controller.collisions.slopeNormal.y;
					velocity.x = maxJumpVelocity * controller.collisions.slopeNormal.x;
				}
			} else {
				velocity.y = maxJumpVelocity;

				if (JumpParticles.isStopped)
				    JumpParticles.Play ();
			}
		}
	}

	public void OnJumpInputUp() {
		if (velocity.y > minJumpVelocity) {
			velocity.y = minJumpVelocity;
		}
	}
		

	void HandleWallSliding() {
		wallDirX = (controller.collisions.left) ? -1 : 1;
		wallSliding = false;
		if ((controller.collisions.left || controller.collisions.right) && !controller.collisions.below && velocity.y < 0) {
			wallSliding = true;

			if (velocity.y < -wallSlideSpeedMax) {
				velocity.y = -wallSlideSpeedMax;
				if (wallSlideParticles.isStopped) {
					wallSlideParticles.Play ();
				}
			} 

			if (controller.collisions.left) {
				playerFacing = 1;
			} else {
				playerFacing = -1;
			}


			if (timeToWallUnstick > 0) {
				velocityXSmoothing = 0;
				velocity.x = 0;

				if (directionalInput.x != wallDirX && directionalInput.x != 0) {
					timeToWallUnstick -= Time.deltaTime;
				}
				else {
					timeToWallUnstick = wallStickTime;
				}
			}
			else {
				timeToWallUnstick = wallStickTime;
			}

		}

	}

	void CalculateVelocity() {
		float targetVelocityX = directionalInput.x * moveSpeed;
		velocity.x = Mathf.SmoothDamp (velocity.x, targetVelocityX, ref velocityXSmoothing, (controller.collisions.below)?accelerationTimeGrounded:accelerationTimeAirborne);
		velocity.y += gravity * Time.deltaTime;
	}
}
