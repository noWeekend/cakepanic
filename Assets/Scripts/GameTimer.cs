﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

using XboxCtrlrInput;

public class GameTimer : MonoBehaviour {


    [SerializeField] private Text timerText;
    [SerializeField] private float roundTime;
    float currentTime;

    [SerializeField] new List<PlayerMovement> players = new List <PlayerMovement>();

    // Use this for initialization
    void Start () {
        currentTime = roundTime;
	}
	
	// Update is called once per frame
	void Update () {
        currentTime -= Time.deltaTime;
        timerText.text = currentTime.ToString("00");

        if(currentTime <= 0)
        {

            GameOver();
            currentTime = 0;
        }
	}

    void GameOver()
    {
        for (int i = 0; i < players.Count; i++)
        {
            if (players[i])
            {
                players[i].HidePlayer();
            }
        }
    }

    void Restart()
    {
        if (XCI.GetButtonDown(XboxButton.A, XboxController.Any))
        {
            Scene scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(scene.name);
        }
    }
}
