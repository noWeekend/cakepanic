// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:32963,y:32812,varname:node_3138,prsc:2|emission-5090-OUT;n:type:ShaderForge.SFN_Color,id:9305,x:31953,y:32591,ptovrint:False,ptlb:Top Colour,ptin:_TopColour,varname:node_1304,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0,c4:0;n:type:ShaderForge.SFN_Color,id:4880,x:31962,y:33199,ptovrint:False,ptlb:Bottom Colour,ptin:_BottomColour,varname:_Color_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:1,c3:0,c4:0;n:type:ShaderForge.SFN_TexCoord,id:362,x:31953,y:32835,varname:node_362,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Multiply,id:5215,x:32211,y:32748,varname:node_5215,prsc:2|A-9305-RGB,B-362-V;n:type:ShaderForge.SFN_OneMinus,id:5509,x:32160,y:32984,varname:node_5509,prsc:2|IN-362-V;n:type:ShaderForge.SFN_Multiply,id:5862,x:32398,y:33108,varname:node_5862,prsc:2|A-5509-OUT,B-4880-RGB;n:type:ShaderForge.SFN_Add,id:5090,x:32649,y:32983,varname:node_5090,prsc:2|A-5215-OUT,B-5862-OUT;proporder:9305-4880;pass:END;sub:END;*/

Shader "Shader Forge/Gradient-Unlit" {
    Properties {
        _TopColour ("Top Colour", Color) = (1,0,0,0)
        _BottomColour ("Bottom Colour", Color) = (0,1,0,0)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _TopColour;
            uniform float4 _BottomColour;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos( v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float3 emissive = ((_TopColour.rgb*i.uv0.g)+((1.0 - i.uv0.g)*_BottomColour.rgb));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
